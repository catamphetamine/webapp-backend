import DB from '../webapp-db'
import dbConfiguration from '../webapp-db/configuration'

import API from '../webapp-internal-api'
import apiConfiguration from '../webapp-internal-api/configuration'

// `STAGE` variable will be accessible at runtime.
function $initialize() {
  const api = new API(apiConfiguration[getConfigStageName(STAGE)])
  const db = new DB(dbConfiguration[getConfigStageName(STAGE)])
  // Make the tools accessible from functions as global variables.
  global.api = api
  global.db = db
}

function getConfigStageName(stage) {
	switch (stage) {
		case 'dev':
			return 'development'
		case 'prod':
			return 'production'
		default:
			return stage
	}
}

// Notifies devs by email about 5XX errors in production/demo.
async function reportError(error, context) {
	if (STAGE !== 'dev' && (!error.httpStatusCode || /5\\d\\d/.test(error.httpStatusCode))) {
		// Uncaught promise rejections have to "stack" for some reason.
		const errorNotificationText = `${context.functionName}\n\n${error.stack || error}\n\nhttps://console.aws.amazon.com/cloudwatch/home?region=us-east-1#logEventViewer:group=${context.logGroupName};stream=${context.logStreamName}`
		// await api.sns.publish('backend-devs', errorNotificationText, {
		// 	subject: 'AWS Lambda Error',
		// 	prefix: false
		// })
	}
}